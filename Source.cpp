#include <iostream>
#include <string>

using namespace std;

string GetTextFromUser();
int GetNumbersFromUser();
void FirstHunt();
void WhatAreYou();
void WhoAreYou();
void WhatDoYouLookLikeColor();
void WhatDoYouLookLikeSkin();
void Fight();
void Dwarves();
void Elves();
void Humans();
void Goblins();

string text;
string name;
int num;

void main()
{
	cout << "Here marks the start of your adventure!!\n\n";
	cout << "Today is the day You finally turned 500!\n";
	cout << "After all your hard training you will finally be able to go out hunting\n";
	system("pause");
	system("CLS");
	cout << "What will you hunt first?\n\n";
	cout << "1: Sheep\n";
	cout << "2: Frog\n";
	cout << "3: Cow\n";
	cout << "4: Unicorn\n";
	FirstHunt();
	cout << "What do you think you are?\n";
	cout << "1: Lion\n";
	cout << "2: Bear\n";
	cout << "3: Dwarf\n";
	cout << "4: Dragon\n";
	WhatAreYou();
	cout << "You're a dragon you get that now right?\n";
	cout << "Oh yeah I forgot to ask what is your name?\n";
	WhoAreYou();
	cout << "Alright " << name << " What do you look like?\n";
	cout << "What color are you?\n";
	cout << "1: Black and Red\n";
	cout << "2: White and Gold\n";
	cout << "3: Blue and Purple\n";
	cout << "4: Green and Yellow\n";
	WhatDoYouLookLikeColor();
	cout << "Now that that's out of the way what type of skin do you have?\n";
	cout << "1: Scaly\n";
	cout << "2: Feathery\n";
	cout << "3: Leathery\n";
	cout << "4: Spiky\n";
	WhatDoYouLookLikeSkin();
	cout << "As you are about to leave to start your first hunt...\n";
	cout << "You hear the attack horn go off...\n";
	cout << "Looks like the hunt is cancelled... Sorry\n";
	cout << "Time to fight!\n";
	cout << "What is attacking?\n";
	cout << "1: Dwarves\n";
	cout << "2: Elves\n";
	cout << "3: Humans\n";
	cout << "4: Goblins\n";
	Fight();
	if (num == 1)
	{
		cout << "The dwarves sent their champion to take down the dragon king\n";
	}
	else if (num == 2)
	{
		cout << "The elves sent their greatest magician to take down the dragon king\n";
	}
	else if (num == 3)
	{
		cout << "The humans sent their greatest gamer... Who happened to be from another realm.\n";
	}
	else if (num == 4)
	{
		cout << "The goblins send two squadrons of 500 goblins\n";
	}
	cout << "You rush to your kings defense!\n";
	cout << "How do you want to fight him?\n";
	cout << "1: Tail Swipe\n";
	cout << "2: Bite\n";
	cout << "3: Talk it out\n";
	cout << "4: Wind Gust\n";
	if (num == 1)
	{
		Dwarves();
	}
	else if (num == 2)
	{
		Elves();
	}
	else if (num == 3)
	{
		Humans();
	}
	else if (num == 4)
	{
		Goblins();
	}
}

int GetNumbersFromUser()
{
	string testNum;
	getline(cin, testNum);
	try
	{
		num = stoi(testNum);
	}
	catch (...)
	{
		cout << "Input must be a number!\n";
		GetNumbersFromUser();
	}
	
	return num;
}

string GetTextFromUser()
{
	getline(cin, text);
	return text;
}

void FirstHunt()
{
	GetNumbersFromUser();
	if (num == 1)
	{
		cout << "That's so cliche... Do you even know what you are?\n";
	}
	else if (num == 2)
	{
		cout << "Really you chose " << num << "?\n";
		cout << "Do you not know what you are?\n";
	}
	else if (num == 3)
	{
		cout << "Not a bad choice some nice hamburger!\n";
		cout << "Oh yeah do you know what you are?\n";
	}
	else if (num == 4)
	{
		cout << "That's a bit ambitious for your first hunt but I like your taste!\n";
		cout << "You don't remember what you are, do you?\n";
	}
	else
	{
		cout << "What were you thinking would happen?\n";
		cout << "Pick a number between 1-4...\n";
		FirstHunt();
	}
	system("pause");
	system("CLS");
}

void WhatAreYou()
{
	GetNumbersFromUser();
	if (num == 1)
	{
		cout << "A lion really your 500!\n";
	}
	else if (num == 2)
	{
		cout << "A bear? No no no not even close!\n";
	}
	else if (num == 3)
	{
		cout << "Close but also so far from right!\n";
	}
	else if (num == 4)
	{
		cout << "You guessed it!\n";
	}
	else
	{
		cout << "Seriously this is an easy concept just pick one of the options?\n";
		cout << "Pick a number between 1-4...\n";
		WhatAreYou();
	}
	system("pause");
	system("CLS");
}

void WhoAreYou()
{
	GetTextFromUser();
	name = text;
	cout << name <<" that's really your name?\n";
	cout << "Okay if you say so...\n";
	system("pause");
	system("CLS");
}

void WhatDoYouLookLikeColor()
{
	GetNumbersFromUser();
	if (num == 1)
	{
		cout << "Solid choice! You must be hard to see at night!\n";
	}
	else if (num == 2)
	{
		cout << "Shiny and bright! You must be hard to see in the clouds!\n";
	}
	else if (num == 3)
	{
		cout << "Dark but beautiful! You must be hard to see over the ocean!\n";
	}
	else if (num == 4)
	{
		cout << "An interesting mix! You must be hard to see near the land!\n";
	}
	else
	{
		cout << "Okay how many times are we gonna do this?\n";
		cout << "Pick a number between 1-4...\n";
		WhatDoYouLookLikeColor();
	}
	system("pause");
	system("CLS");
}

void WhatDoYouLookLikeSkin()
{
	GetNumbersFromUser();
	if (num == 1)
	{
		cout << "Classic choice! Both pretty and effective!\n";
	}
	else if (num == 2)
	{
		cout << "Fair enough! Soft but maneuverable\n";
	}
	else if (num == 3)
	{
		cout << "What? that can't be comfortable how do you move?\n";
	}
	else if (num == 4)
	{
		cout << "Yikes! Offensive but uncomfortable...\n";
	}
	else
	{
		cout << "Are you kidding me you're this far in and still don't get it?\n";
		cout << "Pick a number between 1-4...\n";
		WhatDoYouLookLikeSkin();
	}
	system("pause");
	system("CLS");
}

void Fight()
{
	GetNumbersFromUser();
	if (num == 1)
	{
		cout << "Dwarves are strong and well versed in combat!\n";
	}
	else if (num == 2)
	{
		cout << "Elves are agile and well versed in magic!\n";
	}
	else if (num == 3)
	{
		cout << "Humans are weak but smart\n";
	}
	else if (num == 4)
	{
		cout << "Goblins are small but there's so many of them!\n";
	}
	else
	{
		cout << "Okay this is getting ridiculous...\n";
		cout << "Pick a number between 1-4...\n";
		Fight();
	}
	system("pause");
	system("CLS");
}

void Dwarves()
{
	GetNumbersFromUser();
	if (num == 1)
	{
		cout << "You go to swipe the dwarf with your tail\n";
		cout << "Unfortunately he had placed a trap and it cut your tail off...\n";
		cout << "Causing you to bleed to death...";
	}
	else if (num == 2)
	{
		cout << "You go to chomp down of the dwarf he sinks into the ground\n";
		cout << "This breaks all of your teeth...\n";
		cout << "This allows him to appear at your chest and stab you in the heart killing you \n";
	}
	else if (num == 3)
	{
		cout << "You ask the dwarf why he has come to kill the king.\n";
		cout << "He explains that the dragons are destroying their home to make nests.\n";
		cout << "You respond by informing him that the dragons were told that the area was uninhabited.\n";
		cout << "You promise that they will no longer make nests there and will help repair the area.\n";
		cout << "The attack was resolved without conflict and you went about your life.\n";
	}
	else if (num == 4)
	{
		cout << "You create a big gust of wind throwing the dwarf back into the wall\n";
		cout << "Killing him instantly... You started a war...\n";
	}
	else
	{
		cout << "Okay your doing this on purpose aren't you?\n";
		cout << "Pick a number between 1-4...\n";
		Dwarves();
	}
	system("pause");
	system("CLS");
}

void Elves()
{
	GetNumbersFromUser();
	if (num == 1)
	{
		cout << "The elf dodges under your tail and summons an icicle and thrusts it into your heart\n";
		cout << "You die... and the elf proceeded to kill the king\n";
	}
	else if (num == 2)
	{
		cout << "You go to bite the elf but he jumped on your head and stabbed you in both eyes and replaced your brain with acid...\n";
		cout << "In case it wasn't clear you died...\n";
	}
	else if (num == 3)
	{
		cout << "You ask the elf why he has come to kill the king.\n";
		cout << "He explains that the dragons are siphoning magic from the elves.\n";
		cout << "You respond by informing him that the dragons were not aware of this.\n";
		cout << "You promise that they would make sure to figure out the cause and help stop it.\n";
		cout << "The attack was resolved without conflict and you went about your life.\n";
	}
	else if (num == 4)
	{
		cout << "You go to hit the elf a blast of air but it seems to have no effect...\n";
		cout << "The elf then kills you with a quick summon of stalagmites and stalactites crushing your body\n";
	}
	else
	{
		cout << "Okay your doing this on purpose aren't you?\n";
		cout << "Pick a number between 1-4...\n";
		Elves();
	}
	system("pause");
	system("CLS");
}

void Humans()
{
	GetNumbersFromUser();
	if (num == 1)
	{
		cout << "You go to hit them with your tail but they dodge and end up turning you into a kabob... tasty...\n";
		cout << "You died...\n";
	}
	else if (num == 2)
	{
		cout << "You go to bite them but they jump back and stab through your head disconnecting your brain...\n";
		cout << "You live but its not much of a life...\n";
	}
	else if (num == 3)
	{
		cout << "You ask the human why he has come to kill the king.\n";
		cout << "He explains that the dragons are eating all of the sheep which making humans starve.\n";
		cout << "You respond by informing him that that was due to some fledgling dragons getting out.\n";
		cout << "You promise that they have been punished and it won't happen again.\n";
		cout << "The attack was resolved without conflict and you went about your life.\n";
	}
	else if (num == 4)
	{
		cout << "You go to hit them with a gust of wind but they bring out a shield and block the hit.\n";
		cout << "Then they charge you and stab you in the heart killing you\n";
	}
	else
	{
		cout << "Okay your doing this on purpose aren't you?\n";
		cout << "Pick a number between 1-4...\n";
		Humans();
	}
	system("pause");
	system("CLS");
}

void Goblins()
{
	GetNumbersFromUser();
	if (num == 1)
	{
		cout << "You hit one squadron with your tail killing half of them but the other half climbed on you\n";
		cout << "They shived you to death...\n";
	}
	else if (num == 2)
	{
		cout << "You managed to bite one of the squadrons killing some but the other squadron got on your head.\n";
		cout << "They deployed ladders off of you and repeatedly stabbed you in the heart.\n";
	}
	else if (num == 3)
	{
		cout << "talk\n";
	}
	else if (num == 4)
	{
		cout << "You hit the first squadron with a mighty gust killing most of them but the other squadron surrounded your feet.\n";
		cout << "They trip you and Stomp your body to death.\n";
	}
	else
	{
		cout << "Okay your doing this on purpose aren't you?\n";
		cout << "Pick a number between 1-4...\n";
		Goblins();
	}
	system("pause");
	system("CLS");
}